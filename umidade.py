import numpy
import matplotlib.pyplot as plt
import pandas
import math
from keras.layers import Dense
from keras.layers import Input
import keras.layers.merge
from keras.models import Model
from LocalMinMaxScaler import MinMaxScaler
from sklearn.metrics import mean_squared_error

numpy.random.seed(7)

dataframe = pandas.read_csv('data_selection.csv', usecols=['UmidadeRelativaMedia'], engine='python' )
dataset_umidade_media = dataframe.values
dataset_umidade_media = dataset_umidade_media.astype('float32')

dataframe = pandas.read_csv('data_selection.csv', usecols=['TempMaximaMedia'], engine='python')
dataset_temp_max = dataframe.values
dataset_temp_max = dataset_temp_max.astype('float32')

dataframe = pandas.read_csv('data_selection.csv', usecols=['TempMinimaMedia'], engine='python' )
dataset_temp_min = dataframe.values
dataset_temp_min = dataset_temp_min.astype('float32')

# criar escaladores
umidade_media_scaler = MinMaxScaler()
temp_max_scaler = MinMaxScaler()
temp_min_scaler = MinMaxScaler()

safe_margin = 20

# calcular máximos e mínimos e armazenar em cache os transformadores
umidade_media_scaler = umidade_media_scaler.fit(dataset_umidade_media,safe_margin)
temp_max_scaler = temp_max_scaler.fit(dataset_temp_max,safe_margin)
temp_min_scaler = temp_min_scaler.fit(dataset_temp_min,safe_margin)

# transformar os dados (normalizacao)
dataset_umidade_media = umidade_media_scaler.transform(dataset_umidade_media)
dataset_temp_max = temp_max_scaler.transform(dataset_temp_max)
dataset_temp_min = temp_min_scaler.transform(dataset_temp_min)

look_back = 12
num_neuronio_oculto = 15

# split into train and test sets
train_size = int(len(dataset_temp_max) * 0.7)
test_size = len(dataset_temp_max) - train_size

train_umidade_media, test_umidade_media = dataset_umidade_media[0:train_size,:], dataset_umidade_media[train_size-look_back:len(dataset_umidade_media),:]
train_temp_max, test_temp_max = dataset_temp_max[0:train_size,:], dataset_temp_max[train_size-look_back:len(dataset_temp_max),:]
train_temp_min, test_temp_min = dataset_temp_min[0:train_size,:], dataset_temp_min[train_size-look_back:len(dataset_temp_min),:]


# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset)-look_back-1):
        a = dataset[i:(i+look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return numpy.array(dataX), numpy.array(dataY)

# reshape into X=t and Y=t+1


# conjuntos de treinamento
train_umidade_media_X, train_umidade_media_Y = create_dataset(train_umidade_media, look_back)
train_temp_max_X, train_temp_max_Y = create_dataset(train_temp_max, look_back)
train_temp_min_X, train_temp_min_Y = create_dataset(train_temp_min, look_back)

# conjuntos de test
test_umidade_media_X, test_umidade_media_Y = create_dataset(test_umidade_media, look_back)
test_temp_max_X, test_temp_max_Y = create_dataset(test_temp_max, look_back)
test_temp_min_X, test_temp_min_Y = create_dataset(test_temp_min, look_back)


# modelo da rna
# entrada: temp max e temp min e suas respectivas janelas
# oculta: 1 neurônio? a ser testado
# saída: temp max e temp min, somente 1 valor

ativacao = 'sigmoid'

umidade_media_input = Input(shape=(look_back,),name='umidade_media_input')
temp_max_input = Input(shape=(look_back,),name='temp_max_input')
temp_min_input = Input(shape=(look_back,),name='temp_min_input')

merged = keras.layers.concatenate([umidade_media_input,temp_max_input,temp_min_input])
x = Dense(num_neuronio_oculto,activation=ativacao)(merged)

temp_max_output = Dense(1,activation=ativacao, name='temp_max_output')(x)
temp_min_output = Dense(1,activation=ativacao, name='temp_min_output')(x)

model = Model(inputs=[umidade_media_input,temp_max_input,temp_min_input], outputs=[temp_max_output,temp_min_output])

model.compile(loss='mean_squared_error', optimizer=keras.optimizers.SGD(lr=0.05))


model.fit({ 'umidade_media_input': train_umidade_media_X ,'temp_max_input': train_temp_max_X, 'temp_min_input': train_temp_min_X}, {'temp_max_output': train_temp_max_Y, 'temp_min_output': train_temp_min_Y}, epochs=10000, verbose=2, batch_size=2)

# gerar predições
trainPredict = model.predict({'umidade_media_input': train_umidade_media_X ,'temp_max_input': train_temp_max_X, 'temp_min_input': train_temp_min_X},batch_size=2)
testPredict = model.predict({'umidade_media_input':test_umidade_media_X,'temp_max_input':test_temp_max_X,'temp_min_input':test_temp_min_X},batch_size=2)

# inverter as predições
# na saída de predict, [0] é temp_max e [1] é temp_min
testPredict[0] = temp_max_scaler.inverse_transform(testPredict[0])
testPredict[1] = temp_min_scaler.inverse_transform(testPredict[1])

temp_max = numpy.ravel(testPredict[0])
temp_min = numpy.ravel(testPredict[1])

test_temp_max_Y = temp_max_scaler.inverse_transform([test_temp_max_Y])
test_temp_min_Y = temp_min_scaler.inverse_transform([test_temp_min_Y])

test_temp_max_Y = numpy.ravel(test_temp_max_Y)
test_temp_min_Y = numpy.ravel(test_temp_min_Y)

temp_max_absolute_error = numpy.absolute(test_temp_max_Y - temp_max)
temp_min_absolute_error = numpy.absolute(test_temp_min_Y - temp_min)

rng = pandas.date_range('11/2009',periods=79,freq='M')

data = pandas.DataFrame({'temperatura maxima media prevista':temp_max,'temperatura minima media prevista':temp_min, 'temperatura maxima media real': test_temp_max_Y, 'temperatura minima media real': test_temp_min_Y, 'temp maxima media erro absoluto': temp_max_absolute_error, 'temp minima media erro absoluto': temp_min_absolute_error},index=rng)
address = "umidade/{}.xlsx".format(num_neuronio_oculto)
data.to_excel(address)
